# Projeto Login

A ideia do projeto é realizar uma tela de login com nome de usuário e senha para treinar as habilidades adquiridas até o momento com os estudos em HTML e CSS. 
Ênfase no treino do uso de formulários, Media Queries com tela de login responsiva e a versão Mobile First.

## Design

Deve ter uma foto (pré-definida no projeto) e o formulário de login (usuário e senha), além do texto explicativo da tela de login.
O formulário deve conter ícones personalizados de usuário e de senha (pré-definidos no projeto).
Deve ter o botão Entrar e o botão Esqueci a senha.
Também será responsivo de acordo com o tamanho das telas e terá características específicas para cada tamanho:
    - telas pequenas e celular: background com cor sólida, imagem acima dos dados de login.
    - tablet: background em degradê, imagem à esquerda dos dados de login.
    - desktop: background em degradê, imagem à direita dos dados de login.

### Texto da Tela de Login

'Seja bem vindo (a) novamente! Faça login para acessar sua conta e poder fazer as configurações no seu ambiente.'
